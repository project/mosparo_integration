Drupal.behaviors.mosparoInitializeFrontend = {
  attach: (context, settings) => {
    jQuery('.drupal-mosparo-container[data-processed="false"]').each(
      function () {
        const boxId = jQuery(this).prop('id');
        const connectionId = jQuery(this).data('mosparo-connection-id');

        if (!(connectionId in settings.mosparo_integration)) {
          const e =
            'Connection settings for mosparo not found. Please check your settings.';
          jQuery(this).text(e).data('data-processed', 'true');
          return;
        }

        let designMode = false;
        if (jQuery(this).data('design-mode')) {
          designMode = true;
        }

        const connection = settings.mosparo_integration[connectionId];

        if (typeof mosparo !== 'function') {
          return;
        }

        // eslint-disable-next-line new-cap, no-undef
        const mosparoElement = new mosparo(
          boxId,
          connection.host,
          connection.uuid,
          connection.publicKey,
          { loadCssResource: true, designMode }
        );

        jQuery(this)
          .attr('data-processed', 'true')
          .data('mosparo', mosparoElement);
      }
    );
  },
};
