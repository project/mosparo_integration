<?php

namespace Drupal\mosparo_contact\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\mosparo_integration\MosparoConnectionInterface;
use Drupal\mosparo_integration\Service\MosparoServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'mosparo_default' widget.
 *
 * @FieldWidget(
 *   id = "mosparo_default",
 *   label = @Translation("mosparo"),
 *   field_types = {
 *     "mosparo_contact"
 *   }
 * )
 */
class MosparoWidget extends WidgetBase {

  /**
   * The EntityStorage for the mosparo connection entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $entityStorage;

  /**
   * The mosparo service to help with the mosparo integration.
   *
   * @var \Drupal\mosparo_integration\Service\MosparoServiceInterface
   */
  protected ?MosparoServiceInterface $mosparoService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->entityStorage = $container->get('entity_type.manager')->getStorage('mosparo_connection');
    $instance->mosparoService = $container->get('mosparo_integration.service');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    if (!$this->getFieldSetting('mosparo_connection')) {
      return $element;
    }

    /** @var \Drupal\mosparo_integration\MosparoConnectionInterface $connection */
    $connection = $this->entityStorage->load($this->getFieldSetting('mosparo_connection'));
    if (!$connection) {
      return $element;
    }

    $designMode = FALSE;
    $buildInfo = $form_state->getBuildInfo();
    if (isset($buildInfo['base_form_id']) && $buildInfo['base_form_id'] === 'field_config_form') {
      $designMode = TRUE;
    }

    $fieldData = [];
    if ($connection !== NULL) {
      $attached = $this->mosparoService->generateAttached($connection);

      $fieldData = [
        '#markup' => $this->mosparoService->generateHtml($connection, $designMode),
        '#attached' => $attached,
      ];
    }

    $form['#validate'][] = function ($form, $form_state) use ($connection) {
      $this->validateMosparoField($form, $form_state, $connection);
    };

    $element['value'] = $element + $fieldData;

    return $element;
  }

  /**
   * Validates the form and the mosparo field.
   *
   * @param array $form
   *   The array with the form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\mosparo_integration\MosparoConnectionInterface $connection
   *   The mosparo connection.
   *
   * @return bool
   *   TRUE, if the field is valid. FALSE if not.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *   If the complex data structure is unset and no property can be created.
   */
  public function validateMosparoField(array $form, FormStateInterface &$form_state, MosparoConnectionInterface $connection) {
    $fieldInformation = $this->mosparoService->extractFieldInformation($form_state);

    [
      $formData,
      $requiredFields,
      $verifiableFields,
      $submitToken,
      $validationToken,
    ] = $this->mosparoService->prepareFormData(
      $form_state->getUserInput(),
      $fieldInformation
    );

    $form_error_handler = \Drupal::service('form_error_handler');

    $client = $this->mosparoService->getApiClient($connection);

    try {
      /** @var \Mosparo\ApiClient\VerificationResult $res */
      $res = $client->verifySubmission($formData, $submitToken, $validationToken);

      if ($res !== NULL) {
        $verifiedFields = array_keys($res->getVerifiedFields());
        $requiredFieldDifference = array_diff($requiredFields, $verifiedFields);
        $verifiableFieldDifference = array_diff($verifiableFields, $verifiedFields);

        if ($res->isSubmittable() && empty($requiredFieldDifference) && empty($verifiableFieldDifference)) {
          return TRUE;
        }
        else {
          $form_state->setErrorByName($this->getBaseId(), t('The form is not submittable.'));
          $form_error_handler->handleFormErrors($form, $form_state);
          $form_state->setValidationComplete();
        }
      }
      else {
        foreach ($res->getIssues() as $issue) {
          $form_state->setErrorByName($this->getBaseId(), t('mosparo returned an error. Error: :message', [':message' => $issue['message']]));
          $form_error_handler->handleFormErrors($form, $form_state);
          $form_state->setValidationComplete();
        }
      }
    }
    catch (\Exception $e) {
      $form_state->setErrorByName($this->getBaseId(), $e->getMessage());
      $form_error_handler->handleFormErrors($form, $form_state);
      $form_state->setValidationComplete();
    }

    return FALSE;
  }

}
