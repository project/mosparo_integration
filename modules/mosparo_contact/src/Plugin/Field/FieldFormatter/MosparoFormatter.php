<?php

namespace Drupal\mosparo_contact\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Implementation of the formatter for mosparo_contact.
 *
 * @FieldFormatter(
 *   id = "mosparo_contact",
 *   label = @Translation("mosparo"),
 *   field_types = {
 *     "mosparo_contact"
 *   }
 * )
 */
class MosparoFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return [];
  }

}
