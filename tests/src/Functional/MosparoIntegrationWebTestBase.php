<?php

namespace Drupal\Tests\mosparo_integration\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * The abstract base class for the functional tests.
 *
 * @group mosparo_integration
 */
abstract class MosparoIntegrationWebTestBase extends BrowserTestBase {

  const ADMIN_URL = 'admin/config/mosparo/connections';

  /**
   * Array with the enabled modules.
   *
   * @var array
   */
  protected static $modules = ['mosparo_integration'];

  /**
   * The default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * The admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    \Drupal::moduleHandler()->loadInclude('mosparo_integration', 'inc');

    $permissions = [
      'access content',
      'administer mosparo',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
  }

}
