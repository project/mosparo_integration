<?php

namespace Drupal\mosparo_integration\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * The form to delete a mosparo connection.
 *
 * @package Drupal\mosparo_integration
 */
class MosparoConnectionDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('mosparo_connection.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()->addMessage($this->t('mosparo Connection %label has been deleted.', ['%label' => $this->entity->label() ?? $this->entity->id()]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
