<?php

namespace Drupal\mosparo_integration\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * List builder for the mosparo connection entity list.
 *
 * @package Drupal\mosparo_integration
 */
class MosparoConnectionListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['mosparoHost'] = $this->t('mosparo Host');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->getLabel();
    $row['mosparoHost'] = $entity->getMosparoHost();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $description = new FormattableMarkup(
      $this->t('There are no mosparo connections.')
      . '<br><br>'
      . $this->t('To use the mosparo plugin, you need a connection to a mosparo project. Learn more about the next steps on our website.')
      . '<br><br>'
      . '<a href="https://mosparo.io/how-to-use/" class="button button--small" target="_blank">' . $this->t('Read more') . '</a>',
      []
    );

    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#empty' => $description,
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][$entity->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

}
