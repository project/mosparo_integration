<?php

namespace Drupal\mosparo_integration\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\mosparo_integration\MosparoConnectionInterface;

/**
 * Defines the MosparoConnection entity.
 *
 * @ConfigEntityType(
 *   id = "mosparo_connection",
 *   label = @Translation("mosparo Connection"),
 *   handlers = {
 *     "list_builder" = "Drupal\mosparo_integration\Controller\MosparoConnectionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\mosparo_integration\Form\MosparoConnectionForm",
 *       "edit" = "Drupal\mosparo_integration\Form\MosparoConnectionForm",
 *       "delete" = "Drupal\mosparo_integration\Form\MosparoConnectionDeleteForm"
 *     }
 *   },
 *   config_prefix = "mosparo_connection",
 *   admin_permission = "administer mosparo",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "mosparoHost",
 *     "mosparoUuid",
 *     "mosparoPublicKey",
 *     "mosparoPrivateKey",
 *     "verifySsl",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/mosparo/connections/{mosparo_connection}",
 *     "delete-form" = "/admin/config/mosparo/connections/{mosparo_connection}/delete",
 *   }
 * )
 */
class MosparoConnection extends ConfigEntityBase implements MosparoConnectionInterface {

  /**
   * The id of the connection.
   *
   * @var string
   */
  protected string $id = '';

  /**
   * The label of the connection.
   *
   * @var string
   */
  protected string $label = '';

  /**
   * The mosparo host of the connection.
   *
   * @var string
   */
  protected string $mosparoHost = '';

  /**
   * The mosparo UUID of the connection.
   *
   * @var string
   */
  protected string $mosparoUuid = '';

  /**
   * The mosparo public key of the connection.
   *
   * @var string
   */
  protected string $mosparoPublicKey = '';

  /**
   * The mosparo private key of the connection.
   *
   * @var string
   */
  protected string $mosparoPrivateKey = '';

  /**
   * Defines if the SSL certificates need to be verified or not.
   *
   * @var bool
   */
  protected bool $verifySsl = TRUE;

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function setId(string $id) {
    $this->id = $id;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel(string $label) {
    $this->label = $label;
  }

  /**
   * {@inheritdoc}
   */
  public function getMosparoHost(): string {
    return $this->mosparoHost;
  }

  /**
   * {@inheritdoc}
   */
  public function setMosparoHost(string $mosparoHost) {
    $this->mosparoHost = $mosparoHost;
  }

  /**
   * {@inheritdoc}
   */
  public function getMosparoUuid(): string {
    return $this->mosparoUuid;
  }

  /**
   * {@inheritdoc}
   */
  public function setMosparoUuid(string $mosparoUuid) {
    $this->mosparoUuid = $mosparoUuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getMosparoPublicKey(): string {
    return $this->mosparoPublicKey;
  }

  /**
   * {@inheritdoc}
   */
  public function setMosparoPublicKey(string $mosparoPublicKey) {
    $this->mosparoPublicKey = $mosparoPublicKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getMosparoPrivateKey(): string {
    return $this->mosparoPrivateKey;
  }

  /**
   * {@inheritdoc}
   */
  public function setMosparoPrivateKey(string $mosparoPrivateKey) {
    $this->mosparoPrivateKey = $mosparoPrivateKey;
  }

  /**
   * {@inheritdoc}
   */
  public function shouldVerifySsl(): bool {
    return ($this->verifySsl);
  }

  /**
   * {@inheritdoc}
   */
  public function setVerifySsl(bool $verifySsl) {
    $this->verifySsl = $verifySsl;
  }

  /**
   * Returns the URL to the frontend JavaScript file for the mosparo host.
   *
   * @return string
   *   The URL to the frontend JavaScript.
   */
  public function getFrontendJsUrl(): string {
    return rtrim($this->mosparoHost, '/') . '/build/mosparo-frontend.js';
  }

}
