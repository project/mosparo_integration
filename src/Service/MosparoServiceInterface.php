<?php

namespace Drupal\mosparo_integration\Service;

use Drupal\Core\Form\FormStateInterface;
use Drupal\mosparo_integration\MosparoConnectionInterface;
use Mosparo\ApiClient\Client;

/**
 * Provides an interface for defining a mosparo service.
 */
interface MosparoServiceInterface {

  /**
   * Creates a Client object for the given mosparo connection.
   *
   * @param \Drupal\mosparo_integration\MosparoConnectionInterface $connection
   *   The mosparo connection to create the client for.
   *
   * @return \Mosparo\ApiClient\Client
   *   The created Client for the given MosparoConnection.
   */
  public function getApiClient(MosparoConnectionInterface $connection): Client;

  /**
   * Generates the HTML code for the mosparo box for the given connection.
   *
   * @param \Drupal\mosparo_integration\MosparoConnectionInterface $connection
   *   The mosparo connection.
   * @param bool $designMode
   *   TRUE, if the box should be in the design mode.
   *
   * @return string
   *   The HTML for the mosparo box.
   */
  public function generateHtml(MosparoConnectionInterface $connection, bool $designMode = FALSE): string;

  /**
   * Generates the structure for the 'attached' key.
   *
   * @param \Drupal\mosparo_integration\MosparoConnectionInterface $connection
   *   The mosparo connection for which the information should be generated.
   *
   * @return array
   *   The array with the attached information.
   */
  public function generateAttached(MosparoConnectionInterface $connection): array;

  /**
   * Verifies the submitted data with the mosparo installation..
   *
   * @param array $data
   *   An array with the submitted data.
   * @param array $elements
   *   An array with the form elements.
   * @param array $additionallyIgnoredFields
   *   An array with field keys which should be ignored.
   *
   * @return array
   *   An array with the cleaned data, the required and verifiable fields,
   *   as well as the submit and validation token
   */
  public function prepareFormData(array $data, array $elements, array $additionallyIgnoredFields = []): array;

  /**
   * Extracts the field information from the form structure.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The FormState object.
   * @param array $completeForm
   *   An array with the complete form, if available.
   *
   * @return array
   *   The elements of the form.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   *   If the complex data structure is unset and no property can be created.
   */
  public function extractFieldInformation(FormStateInterface $formState, array $completeForm = []): array;

}
