<?php

namespace Drupal\mosparo_integration\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event to filter the ignored and verifiable field types.
 *
 * @package Drupal\mosparo_integration
 */
class MosparoIntegrationFilterFieldTypesEvent extends Event {

  const EVENT_NAME = 'mosparo_integration.filter_field_types';

  /**
   * The list of the ignored field types.
   *
   * @var array
   */
  protected array $ignoredFieldTypes = [];

  /**
   * The list of the verifiable field types.
   *
   * @var array
   */
  protected array $verifiableFieldTypes = [];

  public function __construct(array $ignoredFieldTypes, array $verifiableFieldTypes) {
    $this->ignoredFieldTypes = $ignoredFieldTypes;
    $this->verifiableFieldTypes = $verifiableFieldTypes;
  }

  /**
   * Returns the ignored field types.
   *
   * @return array
   *   The list of ignored field types.
   */
  public function getIgnoredFieldTypes(): array {
    return $this->ignoredFieldTypes;
  }

  /**
   * Sets the ignored field types.
   *
   * @param array $ignoredFieldTypes
   *   The list of ignored field types.
   *
   * @return $this
   */
  public function setIgnoredFieldTypes(array $ignoredFieldTypes): self {
    $this->ignoredFieldTypes = $ignoredFieldTypes;

    return $this;
  }

  /**
   * Returns the verifiable field types.
   *
   * @return array
   *   The list of verifiable field types.
   */
  public function getVerifiableFieldTypes(): array {
    return $this->verifiableFieldTypes;
  }

  /**
   * Sets the verifiable field types.
   *
   * @param array $verifiableFieldTypes
   *   The list of verifiable field types.
   *
   * @return $this
   */
  public function setVerifiableFieldTypes(array $verifiableFieldTypes): self {
    $this->verifiableFieldTypes = $verifiableFieldTypes;

    return $this;
  }

}
