<?php

namespace Drupal\mosparo_integration\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event to filter the prepared form data.
 *
 * @package Drupal\mosparo_integration
 */
class MosparoIntegrationFilterFormDataEvent extends Event {

  const EVENT_NAME = 'mosparo_integration.filter_form_data';

  /**
   * The form data.
   *
   * @var array
   */
  protected array $formData = [];

  /**
   * The keys of the required fields.
   *
   * @var array
   */
  protected array $requiredFields = [];

  /**
   * The keys of the verifiable fields.
   *
   * @var array
   */
  protected array $verifiableFields = [];

  public function __construct(array $formData, array $requiredFields, array $verifiableFields) {
    $this->formData = $formData;
    $this->requiredFields = $requiredFields;
    $this->verifiableFields = $verifiableFields;
  }

  /**
   * Returns the form data.
   *
   * @return array
   *   The form data as array.
   */
  public function getFormData(): array {
    return $this->formData;
  }

  /**
   * Sets the form data.
   *
   * @param array $formData
   *   The form data as array.
   *
   * @return $this
   */
  public function setFormData(array $formData): self {
    $this->formData = $formData;

    return $this;
  }

  /**
   * Returns the keys of the required fields.
   *
   * @return array
   *   List of keys of the required fields.
   */
  public function getRequiredFields(): array {
    return $this->requiredFields;
  }

  /**
   * Sets the keys of the required fields.
   *
   * @param array $requiredFields
   *   List of keys of the required fields.
   *
   * @return $this
   */
  public function setRequiredFields(array $requiredFields): self {
    $this->requiredFields = $requiredFields;

    return $this;
  }

  /**
   * Returns the keys of the verifiable fields.
   *
   * @return array
   *   List of keys of the verifiable fields.
   */
  public function getVerifiableFields(): array {
    return $this->verifiableFields;
  }

  /**
   * Sets the keys of the verifiable fields.
   *
   * @param array $verifiableFields
   *   List of keys of the verifiable fields.
   *
   * @return $this
   */
  public function setVerifiableFields(array $verifiableFields): self {
    $this->verifiableFields = $verifiableFields;

    return $this;
  }

}
